import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  cart;
  line;
  logo;
  profile;
  search;
  footer;
  
 
    constructor() {
      this.cart = 'assets/images/cart.png',
      this.line = 'assets/images/line.png',
      this.logo = '/assets/images/logo.png',
      this.profile = 'assets/images/profile.png',
      this.search = 'assets/images/search.png',
      this.footer = 'assets/images/Soundscape.io.png'
    }

      
  }
 
