webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = "\n\n.menu{\n     float: right;\n }\n  \nul {\n    list-style-type: none;\n    margin: 0;\n    padding: 2px 0 0 0;\n    overflow: hidden;\n    background-color: #333;\n}\n  \nli {\n    float: left;\n}\n  \nli a {\n     \n    font-family: 'Roboto', sans-serif;\n    font-weight: 500;\n    font-size: .9rem;\n    line-height: 1.25rem;\n      display: block;\n    color: #d4d4d4;\n    padding: 14px 16px;\n    text-decoration: none;\n }\n  \n.active {\n    background-color: gradient;\n}\n  \n.profile{\n     padding: 20px 0 10px 0;\n    font-size: 30px;\n      margin-left: 48%;\n        color: #505050;\n        font-family: 'Raleway', sans-serif !important;\n  }\n  \n.underline {\n     border-bottom: #e25f50 3px solid;\n}\n  \n.subs{\n    font-size: 30px;\n    margin-left: 47%;\n    color: #4f4e4e;\n    font-family: 'Raleway', sans-serif !important;\n  }\n  \n.image{\n    margin-left: 51%;\n }\n  \n.form{\n     width: 50%;\n     margin-left:23%;\n     font-family: sans-serif;\n     padding-bottom: 20px;\n     font-weight: lighter;\n }\n  \n.row {\n    width: 120%; \n    overflow: hidden;\n    padding: 10px;\n}\n  \n.col {\n    float: left;\n    width: 22%;\n    font-size: 15pt;\n      color: #505050;\n}\n  \n.col2{\n    float: left;\n    width: 28%;\n }\n  \ninput[type=submit]{\n    background-color: #019587;  \n    color: #F3EFEF;\n    padding: 10px 22px;\n    margin-left: 48%;\n    font-size: 23px;\n    font-weight: 200;\n   }\n  \n.col.required:after { content:\"*\"; color:red;}\n  \n.button{\n     font-size: 12px;\n    margin-left: 25%;\n    padding-bottom: 20px;\n     \n }\n  \n.button1{\n    background-color: #555555;  \n    color: #E7E5E5;\n    padding: 10px 20px;\n    margin-left: 45px;\n     font-size: 20px;\n     width: 30%;\n     font-weight: 250;\n}\n  \n.plan{\n   margin-left:38%;\n    font-size: 18pt;\n   font-weight: light;\n   line-height: 50px;\n   color: #505050;\n   font-family: sans-serif;\n   font-weight: lighter;\n }\n  \n.a {\n    text-indent: 50px;\n}\n  \nfooter{\n    overflow: hidden;\n    background-color: #333;\n    color: #e1e1e1;;\n     text-decoration: none;\n     padding: 20px;\n }\n  \n.footer{\n    margin-left: 20%;\n    width: 75%;\n    padding-top: 20px;\n    font-family:  sans-serif;\n     \n }\n  \n.col3{\n     \n    float:left; \n    width:33%;\n}\n  \n.row3{\n    display: block;\n    \n }\n  \n.row3 a{\n    display: block;\n    text-decoration: none;\n    color: #e1e1e1;\n    font-size: 17px;\n    font-weight: 400;\n }\n  \n.icons{\n    margin-left: 45%;\n     width: 20%;\n     \n }\n  \na .fa{\n     padding: 5px;\n     text-decoration: none;\n     color: #e1e1e1;;\n }\n  \n.copyright{\n    padding:3% 0 0 33%;\n }\n  \n@media only screen and (max-width: 568px) {\n      \n     \n\n    ul {\n       width: 110%;\n     }\n    .profile{\n         margin-left: 40%;\n     }\n      \n    .subs{\n       font-size: 30px;\n       margin-left: 30%;\n       color: #4f4e4e;\n       font-family: 'Raleway', sans-serif !important;\n     }\n    .image{\n       margin-left: 52%;\n    }\n    .form{\n        display: block;\n        width: 100%;\n        margin-left:10%;\n        font-family: sans-serif;\n         \n    }\n    input[type=submit]{\n        margin-left: 25%;\n       }\n\n   .row {\n    width: 120%; \n    overflow: hidden;\n   }\n    .col{\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n    }\n    \n   .button{\n    padding: 5%;\n    margin-left: 20%;\n    }\n    .button1{\n        \n        width: 90%;\n        display: block;\n        margin-left: 5%;\n        \n    }\n    .plan{\n        margin-left:10%;\n         width: 115%;\n      }  \n      .a {\n        text-indent: 0px;\n    }\n    .icons{\n        margin-left: 25%;\n         display: inline; \n         \n     } \n\n     .copyright{\n        padding:8% 0 0 20%;\n     }\n\n     footer{\n         width: 110%;\n     }\n     .footer{\n        margin-left: 2%;\n        width: 100%;\n         \n     }\n\n     .row3 a{\n         \n        font-size: 13px;\n        font-weight: 400;\n     }   \n}\n  \n@media screen and (min-width: 568px) and (max-width: 768px) {\n    ul {\n        width: 115%;\n      }\n     .profile{\n          margin-left: 50%;\n      }\n       \n     .subs{\n        font-size: 30px;\n        margin-left: 50%;\n        color: #4f4e4e;\n        font-family: 'Raleway', sans-serif !important;\n      }\n     .image{\n        margin-left: 55%;\n     }\n     .form{\n         display: block;\n         width: 100%;\n         margin-left:10%;\n         font-family: sans-serif;\n          \n     }\n     input[type=submit]{\n         margin-left: 40%;\n        }\n \n    .row {\n     width: 120%; \n     overflow: hidden;\n    }\n     .col{\n         display: -webkit-box;\n         display: -ms-flexbox;\n         display: flex;\n     }\n     \n    .button{\n     padding: 5%;\n     margin-left: 20%;\n     }\n     .button1{\n         \n         width: 90%;\n         display: block;\n         margin-left: 5%;\n         \n     }\n     .plan{\n         margin-left:25%;\n          width: 115%;\n       }  \n       .a {\n         text-indent: 110px;\n     }\n     .icons{\n         margin-left: 45%;\n          display: inline; \n          \n      } \n \n      .copyright{\n         padding:8% 0 0 30%;\n      }\n \n      footer{\n          width: 110%;\n      }\n      .footer{\n         margin-left: 15%;\n         width: 100%;\n          \n      }\n \n      .row3 a{\n          \n         font-size: 13px;\n         font-weight: 400;\n      }   \n}\n  "

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<body> \n  <nav>\n    <ul>\n        <div class=logo>\n            <li><a  href=\"#\"><img [src]=\"logo\" ></a></li>\n            <li><a  href=\"#\"><img [src]=\"footer\"></a></li>\n        </div>\n      \n      <div class=\"menu\" >\n        <li><a href=\"#\">Search Music</a></li>\n        <li><a href=\"#\">Submit Music</a></li>\n        <li ><a href=\"#\">Pricing</a></li>\n        <li><a href=\"#\"><img [src]=\"line\"></a></li>\n        <li><a href=\"#\"><img [src]=\"cart\"></a></li>\n        <li><a href=\"#\"><img [src]= \"search\" ></a></li>\n        <li ><a class=\"active\" href=\"#\">My Account</a></li>\n      </div>\n    </ul>\n  </nav>\n\n  <section>\n    <div class=\"profile\">\n        <span class=\"underline\"><span  >My Profile</span></span>\n    </div>\n    <div class=\"image\">\n        \n        <img [src]=\"profile\">\n    </div>\n    \n    <form class=\"form\">\n        <div class=\"row\">\n            <div class=\"col required\">\n                <label>First Name </label>\n                \n            </div>\n            <div class=\"col2\">\n                <input type=\"text\" size=\"30\" required>\n            </div>\n            <div class=\"col required\">\n                <label>Last Name</label>\n                \n            </div>\n            <div class=\"col2\">\n                <input type=\"text\" size=\"30\" required>\n            </div>\n        </div>\n      \n        <div class=\"row\">\n            <div class=\"col required\">\n                <label>Email</label>\n            </div>\n            <div class=\"col2\">\n                <input type=\"text\" size=\"30\" required>\n            </div>\n        </div>\n      \n        <div class=\"row\">\n            <div class=\"col\">\n                <label>Password</label>\n            </div>\n            <div class=\"col2\">\n                <a style=\"color:rgba(255, 0, 0, 0.66);\" href=\"#\">Click to Change Password</a>\n            </div>\n        </div>\n      \n        <div class=\"row\">\n            <div class=\"col required\">\n                <label>Phone</label>\n            </div>\n            <div class=\"col2\">\n                <input type=\"text\" size=\"30\" required>\n            </div>\n            <div class=\"col\">\n                <label>How did you hear about us?</label>\n            </div>\n            <div class=\"col2\">\n                <input type=\"text\" size=\"30\">\n            </div>\n        </div>\n        \n        <div class=\"row\">\n            <div class=\"col\">\n                <label>License Name or Company</label>\n            </div>\n            <div class=\"col2\">\n                <input type=\"text\" size=\"30\" >\n            </div>\n            <div class=\"col\">\n                <label>Website</label>\n            </div>\n            <div class=\"col2\">\n                <input type=\"text\" size=\"30\" >  \n            </div>\n        </div>\n      \n        <div class=\"row\">\n            <div class=\"col\">\n                <label>YouTube channel URL</label>\n            </div>\n            <div class=\"col2\">\n                <input type=\"text\" size=\"30\">\n            </div>\n            <div class=\"col\">\n                <label>Twitch Channel URL</label>\n            </div>\n            <div class=\"col2\">\n                <input type=\"text\" size=\"30\">\n            </div>\n        </div>\n      \n        <input type=\"submit\" value=\"UPDATE INFO\" >\n    </form>\n    \n  </section>\n  <hr style=\"width:58%;\">\n  <section>\n      <div class=\"subs\">\n          <span class=\"underline\"><span  >My Subscription</span></span>\n          \n      </div>\n\n      <div class=\"plan\">\n          <p>\n              You are currently subscribed to the following plan:\n            </p>\n            \n            <p  class=\"a\">\n              PROFESSIONAL - $249/year\n            </p>\n            <p  class=\"a\">\n              Renewal Date: May 25, 2019\n            </p>\n      </div>\n      \n      <div class=\"button\">\n          <button   class=\"button1\"  >UPGRADE NOW</button>\n          <button  class=\"button1\" >CANCEL SUBSCRIPTION</button>\n      </div>\n      \n  </section>\n\n  <footer>\n    <div class=\"icons\" style=\"padding-bottom:20px;\">\n        <a href=\"#\"> <i class=\"fa fa-facebook-square\" style=\"font-size:28px\"></i></a>\n        <a href=\"#\"><i class=\"fa fa-google-plus-square\" style=\"font-size:28px\"></i></a>\n        <a href=\"#\"><i class=\"fa fa-twitter-square\" style=\"font-size:28px\"></i></a>\n        <a href=\"#\"><i class=\"fa fa-instagram\" style=\"font-size:28px\"></i></a>\n        <a href=\"#\"><i class=\"fa fa-linkedin-square\" style=\"font-size:28px\"></i></a>\n    </div>\n    <hr style=\"width:60%;\">\n    <div class=\"footer\">\n        <div class=\"col3\">\n            <label class=\"row3\" ><a href=\"#\">Music</a> </label>\n            <label class=\"row3\" ><a href=\"#\">Music Search</a></label>\n            <label class=\"row3\" ><a href=\"#\">Our Story</a></label>\n            <label  class=\"row3\"><a href=\"#\">Pricing</a></label>\n          </div>\n        \n          <div class=\"col3\">\n              <label class=\"row3\" ><a href=\"#\">Sign Up</a></label>\n              <label class=\"row3\" ><a href=\"#\">Make a Music Request</a></label>\n              <label class=\"row3\" ><a href=\"#\">our Current Music Needs</a></label>\n              <label class=\"row3\"><a href=\"#\">Tv Film credits</a></label>\n            </div>\n        \n        <div class=\"col3\">\n              <label class=\"row3\"><a href=\"#\">Contact Us</a></label>\n              <label class=\"row3\"><a href=\"#\">FAQ</a></label>\n              <label class=\"row3\"><a href=\"#\">Privacy Policy</a></label>\n              <label class=\"row3\"><a href=\"#\">Terms & Conditions</a></label>\n            </div> \n\n            <ul class = \"copyright\"    >\n                <li style=\"padding:0 3px 0 0;\">&copy;2018</li>\n                <li ><img [src]=\"footer\"></li>\n                <li style=\"padding:0 0 0 3px;\">Inc.</li>\n              </ul>\n     </div>\n    \n  </footer>\n</body>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
        this.cart = 'assets/images/cart.png',
            this.line = 'assets/images/line.png',
            this.logo = '/assets/images/logo.png',
            this.profile = 'assets/images/profile.png',
            this.search = 'assets/images/search.png',
            this.footer = 'assets/images/Soundscape.io.png';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("./src/app/app.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["E" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map